# Lager Order Helper

This user script adds several convenience functionalities to make the Thorlabs Order process through the D-PHYS shop easier:

- Prevents you from filling in items on the D-PHYS shop page before logging in (since they will be lost again when logging in)
- Prevents you from accidentally leaving the site when the shopping list is not empty (you'll lose everything when leaving the site)
- Adds a way to automatically add items to the list by clicking a button on the Thorlabs website (see the section on Auto-Fill for more details)

# Installation
- Download the [Tampermonkey](https://www.tampermonkey.net/) extension for your browser
- Install the userscript by clicking [this link](https://gitlab.ethz.ch/langl/lager-order-helper/-/raw/master/LagerShopHelper.user.js)

# Demo of the Auto-Fill functionality
![](AutoFillDemo.gif)

# How to use the Auto-Fill functionality
The user script makes it very easy to add stuff to your shopping list on the D-PHYS shop page:

- Open the [Thorlabs omnibus order page](https://lager.phys.ethz.ch/en/thorlabs/)
- Log in
- Press the `Auto-Fill` button at the top:
  ![](ThorlabsAutofillButton.png)
- Click on the `Lager` button next to the item you want on the Thorlabs website: (optionally after filling in the quantity)
  ![](ThorlabsLagerButton.png)
