// ==UserScript==
// @name         Lager Shop Helper
// @namespace    http://langl.ethz.ch/
// @downloadURL  https://gitlab.ethz.ch/langl/lager-order-helper/-/raw/master/LagerShopHelper.user.js
// @version      0.7
// @description  Utilities for the Thorlabs Order page
// @author       Lukas Lang
// @match        https://lager.phys.ethz.ch/*/thorlabs/
// @match        https://www.thorlabs.com/*
// @match        https://www.thorlabs.de/*
// @grant        GM.deleteValue
// @grant        GM.getValue
// @grant        GM.setValue
// @require      http://code.jquery.com/jquery-3.5.0.min.js
// ==/UserScript==

/* eslint-env jquery */

$(function() {
    'use strict';

    switch (document.location.host)
    {
        case "lager.phys.ethz.ch":
            if ($("input[type='submit'][name='login']").length > 0)
            {
                $('table.artikel').remove();
            }
            else
            {
                $('div.baskettitel').css({display:'inline-block'}).after('<input type="button" value="Auto-fill" id="autofill"/>').after('<pre style="display:inline-block">    </pre>');
            }

            $('table.artikel input').change(function(){
                $('body').attr("onbeforeunload", "return 'Leave site?';");
            });

            $('form').on('submit', function() {
                $('body').attr("onbeforeunload", null);
                unsafeWindow.onbeforeunload = null; //fix for Firefox
            });

            $('input#autofill').click(function() {
                $(this).css({color: '#999'}).prop('disabled', true);

                setInterval(function() {
                    GM.getValue('lagerAutofill').then(function(newData)
                    {
                        if (newData != undefined)
                        {
                            GM.deleteValue('lagerAutofill');
                            try
                            {
                                newData = JSON.parse(newData);

                                newData.forEach(function(i)
                                {
                                    var fields = $('table.artikel tr').filter(function()
                                    {
                                        return $(this).find('th').length == 0 &&
                                            $(this).find('td input').filter(function(){return $(this).val() != "";}).length == 0;
                                    }).first().find('td input');

                                    $(fields[0]).val(i.qty);
                                    $(fields[1]).val(i.id);
                                    $(fields[2]).val(i.desc);
                                    $(fields[3]).val(i.price);

                                    $('body').attr("onbeforeunload", "return 'Leave site?';");
                                });
                            }
                            catch (ex)
                            {}
                        }
                    });
                }, 1000);
            });
            break;
        default:
            $('div.partnumbers tr[class^=AltRow]').each(function(){
                $(this).children('td:first-child').css({'white-space':'nowrap'}).prepend('<input type="button" value="Lager"/>');

                var obj=$(this);

                $(this).find('input[value=Lager]').click(function(){
                    GM.getValue('lagerAutofill', []).then(function(d)
                    {
                        try
                        {
                        d = JSON.parse(d);
                        }
                        catch (ex)
                        {
                            d = [];
                        }

                        if (!Array.isArray(d))
                        {
                            d = [];
                        }

                        d.push({
                            qty:obj.find('td input[type=text][name^="QTY"]').val(),
                            id:obj.find('td.prodNumber a').text(),
                            desc:obj.find('td.prodDesc span[id^="prodTitle"]').text(),
                            price:obj.find('td:contains(€)').text().slice(0,-2).replace(',','.')
                        });

                        GM.setValue('lagerAutofill', JSON.stringify(d));
                    });
                });
            });

            $('form[name=add2Basket]').each(function(){
               $(this).children('input[name=addCart]').after($.parseHTML(' <input type="button" value="Lager"/>'))

                var obj=$(this);

                $(this).find('input[value=Lager]').click(function(){
                    GM.getValue('lagerAutofill', []).then(function(d)
                    {
                        try
                        {
                        d = JSON.parse(d);
                        }
                        catch (ex)
                        {
                            d = [];
                        }

                        if (!Array.isArray(d))
                        {
                            d = [];
                        }

                        d.push({
                            qty:obj.find('input[name=QTY]').val(),
                            id:obj.find('input[name=partNumber]').val(),
                            desc:obj.find('input[name=productTitle]').val(),
                            price:obj.parent().parent().parent().find('font:contains(€)').text().slice(0,-2).replace(',','.')
                        });

                        GM.setValue('lagerAutofill', JSON.stringify(d));
                    });
                });
            });

            $('#myBasket table:first td:nth-child(3)').each(function(){
                $(this).prepend($.parseHTML(' <input type="button" value="Lager"/>'));
                $(this).find('input[value=Lager]').click(function(){
                    GM.getValue('lagerAutofill', []).then(function(d)
                    {
                        try
                        {
                        d = JSON.parse(d);
                        }
                        catch (ex)
                        {
                            d = [];
                        }

                        if (!Array.isArray(d))
                        {
                            d = [];
                        }

                        $('#myCart table tr').each(function(){
                            if ($(this).find('td font b a').text().length > 0)
                            {
                                d.push({
                                    qty:$(this).find('td input[name^=QTY]').val(),
                                    id:$(this).find('td font b a').text(),
                                    desc:$(this).find('td div[id^=prodDesc]').text(),
                                    price:$($(this).find('td:contains(€)').first()[0].childNodes).last().text().slice(0,-2).replace(',','.')
                                });
                            }
                        });

                        GM.setValue('lagerAutofill', JSON.stringify(d));
                    });
                });
            });
    }
});